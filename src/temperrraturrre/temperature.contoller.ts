import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperaturerService: TemperatureService) {}
  @Get('convert') // Handles GET requests to '/convert'
  convert(@Query('celsius') celsius: string) {
    // Retrieves the 'celsius' query parameter
    return this.temperaturerService.convert(parseFloat(celsius));
  }
  @Post('convert') // Handles GET requests to '/convert'
  convertPost(@Body('celsius') celsius: number) {
    // Retrieves the 'celsius' query parameter
    return this.temperaturerService.convert(celsius);
  }
  @Get('convert/:celsius') // Handles GET requests to '/convert'
  convertParam(@Param('celsius') celsius: string) {
    // Retrieves the 'celsius' query parameter
    return this.temperaturerService.convert(parseFloat(celsius));
  }
}
